<?php
// $Id$

/**
 * @file
 * Schema definitions and install / update hooks for Audit module.
 */

/**
 * Implementation of hook_schema().
 */
function audit_schema() {
  $schema = array();
  
  $schema['audit_log'] = array(
    'description' => t('The base table for audit log entries.'),
    'fields' => array(
      'lid' => array(
        'description' => 'The primary identifier for the log message.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'report' => array(
        'description' => 'The identifier for this log report.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'timestamp' => array(
        'description' => 'The Unix timestamp when the log entry was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'title' => array(
        'description' => 'The title of this report entry.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'severity' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
        'description' => 'The severity level of the report; ranges from 0 (Emergency) to 7 (Debug)',
      ),
      'description' => array(
        'type' => 'text',
        'not null' => FALSE,
        'description' => 'A short description of what was performed during this report.'
      ),
      'message' => array(
        'type' => 'text',
        'size' => 'big',
        'description' => 'The full text of this report.',
      ),
    ),
    'primary key' => array('lid'),
    'unique keys' => array(
      'report' => array('report'),
    ),
    'indexes' => array(
      'timestamp' => array('timestamp'),
      'severity' => array('severity'),
    ),
  );
  
  $schema['audit_log_categories'] = array(
    'description' => t('The base table for audit log entries.'),
    'fields' => array(
      'report' => array(
        'description' => 'The identifier for this log report.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'category' => array(
        'description' => 'The identifier for this category.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('report', 'category'),
  );
  
	return $schema;
}

/**
 * Implementation of hook_install().
 */
function audit_install() {
  drupal_install_schema('audit');
}

/**
 * Implementation of hook_install().
 */
function audit_uninstall() {
  drupal_uninstall_schema('audit');
}