<?php
// $Id$

/**
 * @file
 *   Site audit reports for user module.
 */

/**
 * Implementation of hook_audit_report().
 */
function user_audit_report() {
  return array(
    'user-count' => 'user_audit_reports_count',
  );
}

/**
 * Audit report callback.
 */
function user_audit_reports_count() {
  $severity = REQUIREMENT_OK;
  $items = array();

  // Total users
  $total_users = db_result(db_query("SELECT COUNT(*) FROM {users}"));
  $items[] = t('Total users: %total', array('%total' => $total_users));

  // Total users by role
  $roles = user_roles();
  $total_by_role = db_query("SELECT rid AS rid, COUNT(*) AS count FROM {users_roles} GROUP BY rid");
  while ($row = db_fetch_object($total_by_role)) {
    $role = $row->rid;
    $count = $row->count;
    if (!isset($roles[$role])) {
      $role = '<span class="error" title="'. t('Unrecognized role') .'">'. t('%', array('%role' => $role)) .'</span>';
      $severity = REQUIREMENT_WARNING;
    }
    else {
      $role = t('%role', array('%role' => $roles[$role]));
    }
    $items[] = t('Total !role users: %count', array('!role' => $role, '%count' => $count));
  }

  // Sessions
  $total_sessions = db_result(db_query("SELECT COUNT(*) FROM {sessions}"));
  $items[] = t('Total sessions: %total', array('%total' => $total_sessions));

  // Anonymous sessions
  $anon_sessions = db_result(db_query("SELECT COUNT(*) FROM {sessions} WHERE uid = 0"));
  $items[] = t('Anonymous sessions: %total', array('%total' => $anon_sessions));

  // Oldest session
  $oldest_session = db_result(db_query("SELECT MIN(timestamp) FROM {sessions} LIMIT 0,1"));
  $items[] = t('Oldest session: %date', array('%date' => format_date($oldest_session)));

  return array(
    'title' => t('User counts'),
    'value' => t('Details about the number of users on your site'),
    'severity' => $severity,
    'description' => theme('item_list', $items),
  );
}