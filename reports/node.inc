<?php
// $Id$

/**
 * @file
 *   Site audit reports for node module.
 */

/**
 * Implementation of hook_audit_report().
 *
 * Loads reports for the node module. Return format is an
 * array keyed with report name, with each key containing
 * an array with keys 'info', 'status', and 'details'.
 */
function node_audit_report() {
  return array(
    'node-count' => 'node_audit_reports_count',
  );
}

/**
 * Audit report callback.
 */
function node_audit_reports_count($job) {
  watchdog('debug', 'testig...', 'error');
  error_log(print_r($job, true));
  
  $severity = REQUIREMENT_OK;
  $items = array();

  // Total nodes
  $total_nodes = db_result(db_query("SELECT COUNT(*) FROM {node}"));
  $items[] = t('Total nodes: %total', array('%total' => $total_nodes));

  // Total revisions of nodes
  $total_revisions = db_result(db_query("SELECT COUNT(*) FROM {node_revisions}"));
  $items[] = t('Total node revisions: %total', array('%total' => $total_revisions));

  // Total nodes by content type
  $types = node_get_types();
  $total_by_type = db_query("SELECT type AS type, COUNT(*) AS count FROM {node} GROUP BY type");
  while ($row = db_fetch_object($total_by_type)) {
    $type = $row->type;
    $count = $row->count;
    if (!isset($types[$type])) {
      $type = '<span class="error" title="'. t('Unrecognized content type') .'">'. t('%type', array('%type' => $type)) .'</span>';
      $severity = REQUIREMENT_WARNING;
    }
    else {
      $type = t('%type', array('%type' => $types[$type]->name));
    }
    $items[] = t('Total !type nodes: %count', array('!type' => $type, '%count' => $count));
  }

  $options = array(
    'message' => t('Details about the number of nodes on your site'),
    'severity' => $severity,
    'description' => theme('item_list', $items),
  );
  error_log('test');
  audit_insert_log('node-count', t('Node counts'), $options);
  return TRUE;
}