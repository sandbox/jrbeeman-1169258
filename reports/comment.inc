<?php
// $Id$

/**
 * @file
 *   Site audit reports for comment module.
 */

/**
 * Implementation of hook_audit_report().
 */
function comment_audit_report() {
  return array(
    'comment-count' => 'comment_audit_reports_count',
  );
}

/**
 * Audit report callback.
 */
function comment_audit_reports_count() {
  $items = array();

  // Total comments
  $total_comments = db_result(db_query("SELECT COUNT(*) FROM {comments}"));
  $items[] = t('Total comments: %total', array('%total' => $total_comments));

  // Top commented content
  $sql = "SELECT c.nid AS nid, n.title AS title, COUNT(*) AS count
          FROM comments c
          LEFT JOIN {node} n ON n.nid = c.nid
          GROUP BY nid
          ORDER BY count DESC, title ASC
          LIMIT 0,10";
  $top_comments_result = db_query($sql);
  $top_comments = array();
  while ($row = db_fetch_object($top_comments_result)) {
    $title = t('@title (@count)', array('@title' => $row->title, '@count' => $row->count));
    $top_comments[] = l($title, 'node/'. $row->nid);
  }
  $items[] = t('Most commented nodes: !list', array('!list' => implode(', ', $top_comments)));

  return array(
    'title' => t('Comment counts'),
    'value' => t('Details about the number of comments on your site'),
    'severity' => $severity,
    'description' => theme('item_list', $items),
  );
}
