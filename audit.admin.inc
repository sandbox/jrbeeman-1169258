<?php
// $Id$

/**
 * @file
 *   Site audit module administrative pages callbacks.
 */

/**
 * Menu callback.
 *
 * Structured very similarly to system_status() in its output.
 */
function audit_page() {
  include_once './includes/install.inc';
  
  // Load all core reports
  module_load_include('inc', 'audit', 'modules/node');
  module_load_include('inc', 'audit', 'modules/comment');
  module_load_include('inc', 'audit', 'modules/user');
  
  $out = '';
  //$details = module_invoke_all('audit_report');
  $out .= theme('status_report', $details);
  return $out;
}

function audit_page_settings() {
  $form = array();

  $form['audit_reports'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Reports to run'),
    '#options' => array(
      'node' => t('Node'),
      'comment' => t('Comment'),
      'user' => t('User'),
    ),
    '#description' => t('Select which reports you would like to have run for the site audit'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save configuration',
  );

  $form['clear'] = array(
    '#type' => 'submit',
    '#value' => 'Clear the audit log',
  );
  
  return $form;
}

function audit_page_settings_submit($form, $form_state) {
  $values = $form_state['values'];
  $op = $values['op'];

  // Load the report include for each selected report
  if ($op == t('Save configuration')) {
    foreach ($values['audit_reports'] as $key => $selected) {
      if ($selected) {
        module_load_include('inc', 'audit', 'reports/'. $key);
      }
    }
  }

  // Now that we've loaded all the reports, get the list of reports to run and add them to the job scheduler
  $reports = module_invoke_all('audit_report');
  foreach ($reports as $report => $callback) {
    $job = array(
      'callback' => $callback,
      'type' => $report,
      'periodic' => FALSE,
    );
    job_scheduler()->set($job);
  }

  if ($op == t('Clear the audit log')) {
    dpm('redirect to confirmation re: clear the log');
  }
}